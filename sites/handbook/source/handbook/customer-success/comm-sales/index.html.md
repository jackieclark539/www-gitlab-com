---
layout: handbook-page-toc
title: "Commercial Sales - Customer Success"
description: "The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM)"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as worldwide sales for the mid-market and small/medium business segments. [Sales segmentation](/handbook/sales/field-operations/gtm-resources/) is defined by the total employee count of the global account. The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Customer Success Managers (CSM).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role & Responsibilities

### Solutions Architects

Solutions Architects are aligned to the Commercial Sales Account Executives by a pooled model. Requests for an SA will be pulled from the triage board by an SA based on multiple factors including availability, applicable subject matter expertise, and current workload.  

* [Engage a Commercial Solutions Architect](/handbook/customer-success/solutions-architects/processes/commercial/)
* [Solutions Architect role description](/job-families/sales/solutions-architect/)
* [Solutions Architect overview](/handbook/customer-success/solutions-architects/)

### Customer Success Managers

Customer Success Managers that support Commercial Sales are aligned by region (Americas East, Americas West, EMEA, and APAC). Not all accounts will have a dedicated CSM. Account qualification is required.

* [Customer Success Manager role description](/job-families/sales/technical-account-manager/)
* [Customer Success Manager overview](/handbook/customer-success/tam/)
* [When and how a CSM is engaged](/handbook/customer-success/tam/engagement/)

## Sales Engagement Guidelines

Sales engagement for all Commercial Sales is documented in the [Commercial Process](/handbook/customer-success/solutions-architects/processes/commercial/) page.

## Customer Engagement Guidelines

### Ongoing Customer Communication

For commercial accounts, we do not currently offer our customers Slack channels unless they meet specific criteria and it would be highly beneficial to both the customer and GitLab teams. Prior to creating the channel and provisioning access, the CSM for the account must approve its creation and use. To qualify, the customer should be at minimum:
- Premium tier or above
- 100k+ IACV

[Collaborative projects](/handbook/customer-success/tam/engagement/#managing-the-customer-engagement) may be suggested with few possible exceptions (such as reference customers, early adopters or strategic partnerships).  If the customer is evaluating GitLab or doing a POV for an upgrade, SAs or CSMs can request an ephemeral Slack channel, but the channel should be closed within 90 days if it does not meet the criteria shown above. Slack channels are simply too intensive to scale for Mid-Market and SMB.

It is also not scalable for every customer to have a collaboration project, and the CS team will determine the need for a project on a per-customer basis.

## Seamless Customer Journey

A seamless customer journey requires a continuous flow of relevant information between the roles at GitLab with customer outcomes in focus. Below are some examples of the transfer of information between roles that may be required.

### CSM to CSM (existing accounts)

* Ensure all applicable accounts have a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Update CSM name on account team in Salesforce
* Share any Outreach sequences or templates currently in use
* The new CSM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial CSM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/-/boards/1139879?&label_name[]=Status%3A%3ANew)
* Evaluate the state of the account's license including an assessment of active users and if a true-up is necessary
* Evaluate the state of tickets to ensure any open critical issues are addressed
* Retrieve any architectural diagrams, machine specifications, and gitlab configuration files to assess current environment
* Review the [Account Handoff](/handbook/customer-success/tam/account-handoff/) checklist for additional info

### Account Executive to CSM (existing accounts without a CSM)

* AE or SA qualifies the account [meets the threshold for CSM services](/handbook/customer-success/tam/tam-manager/#account-assignment) and raises the account requiring a CSM in the SAL-SA-CSM meeting. CSM managers also review their 'accounts requiring CSM' dashboard in Gainsight on a weekly basis as a fail-safe.
* SA or CSM creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial) if applicable
* Add CSM name to account team in Salesforce
* Identify any relevant Outreach sequences or templates
* The new CSM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial CSM Triage board](https://gitlab.com/gitlab-com/customer-success/tam-triage-boards/commercial/-/issues/new)  

### SA to CSM (new accounts)

When an opportunity reaches stage `7-Negotiating` and is above the [account value threshold](https://about.gitlab.com/handbook/customer-success/tam/services/#commercial), the Commercial CSM Manager will be automatically flagged for a new account to be assigned to a CSM. Once a CSM is assigned to the account, the CSM will arrange a Technical Internal Plan meeting with the SA to review account notes. Prior to the Technical Internal Plan meeting, the following are expected to be updated:
  * The Command Plan for the opportunity should be up to date on their Why Now and specific pain points in Identify Pain.
  * The Custom Pitch Deck (attached to the opportunity) ought to provide current state assessments of their environment and initial plans for adoption of GitLab.  
  * Raw notes available in the Activity History in Salesforce to provide the CSM any additional context necessary.

After the Technical Internal Plan meeting, the AE is to arrange a transition meeting with the customer that includes both the SA and CSM. This meeting will at least cover the communication plan between the CSM and customer going forward.


### SA to SA (new accounts)

* Update SA name on account team in Salesforce
* Ensure any account notes, [tech stack discoveries](https://docs.google.com/spreadsheets/d/1sOeluQhMO4W0wWIC6rbSE_E1NzTj7eTaR-FDKLYlLb4/edit#gid=912439232), [technical briefs](https://gitlab.com/gitlab-com/customer-success/tko/technical-followup-briefs/-/tree/master) or running call notes are linked to Salesforce and shared with the new SA
* Introduce new SA live on a client call
* If a [POV](/handbook/customer-success/solutions-architects/tools-and-resources/pov) is pending or active, update the POV record in Salesforce as required
* Ensure any current action items are identified via an issue on the [Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/-/boards/1006966)
