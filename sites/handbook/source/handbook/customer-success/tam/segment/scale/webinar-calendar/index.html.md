---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of September.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### September 2022

### Intro to GitLab
#### September 7th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yDIlG884SISbzaTyolRWSA)

### Intro to GitLab Office Hours
#### September 7th, 2022 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

In our office hours following our Intro to GitLab webinar, we will focus on questions that are commonly asked by new users of GitLab.
To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/customer-success/scale-office-hours/-/issues/1). 
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Wf0_IvT1RMKdMsaOe0nYLQ)

### Intro to CI/CD 
#### September 13th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_RUr9MxU6SEa7H2rhZ-2YEQ)

### Intro to CI/CD Office Hours
#### September 13th, 2022 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

In our office hours following our Intro to CI/CD webinar, we will focus on questions that are commonly asked by new users of CI/CD with GitLab.
To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/customer-success/scale-office-hours/-/issues/2). 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BqVPDefxTpu5R4r8tvJgvA)

### Advanced CI/CD
#### September 21st, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BXFJdbsNRRq2ii-cW4mAOg)

### DevSecOps/Compliance
#### September 27th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_vowi9PkyRQWpJUHuU93CJw)

### Getting Started with Continuous Delivery and GitOps
#### September 29th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

In this webinar you will learn the concepts behind continuous delivery, continuous deployment and GitOps. You will also learn how to set up a DevOps pipeline that deploys your application to several environments on Kubernetes.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_S7hSXhryT4eTwWxH1lCFgQ)

